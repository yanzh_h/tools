#! /bin/bash
add_yaml(){
#sync your fork
if [[ ! -d $tmpy ]];then
	mkdir -p $tmpy
fi

# this is absolute path nono pushd
cd $tmpy
echo "Hint: yaml hub lives $tmpy"
git clone git@gitee.com:${gitee_id}/infrastructure.git -b master
cd infrastructure/repository
git remote add openeuler git@gitee.com:openeuler/infrastructure.git -f
git checkout -b oe openeuler/master  
git push origin oe:master -f
git checkout master 
git pull -f
echo "Hint: your yaml fork is updated,we are now at `pwd`"

echo -n "Hint: add_yaml for $@ , are you sure? please enter 'yes/yes' or 'no/No': "
read doo
if [[ "$doo" != 'yes' && "$doo" != 'Yes' ]];then
	echo "Hint: you chose to exit :( "
	exit 2
fi

file_o='src-openeuler.yaml'
file='src-openeuler.yaml.cp'
cp -f $file_o $file 
results='add'
# must use $@
for x in "$@";do
	a=$x
	cat $file | grep -w "${a}" &>/dev/null
	if [[ $? == 0 ]];then
		echo "$a has already existed,please check $file_o"
		continue
	fi
	
	dude=`(cat $file && echo "- name: ${a}")|grep '^- name.*'|sort |grep -w $a -A1 |grep -v -w $a| cut -d' ' -f3`
	targ=`cat -n $file |grep -w "$dude"| head -n1 |awk '{print $1}'`
	cat $file | sed "${targ}i\- name: $a\n  description: \"\"\n  type: private" >${file}.tmp 
	cp -f ${file}.tmp ${file}
	results=$results:$a
done
rm -f ${file}.tmp

echo -n "Hint: diff -Nur ${file_o} ${file}, please enter 'yes/Yes' or 'no/No': " 
read diff
if [[ $diff == 'yes' || $diff == 'Yes' ]];then
	diff -Nur ${file_o} ${file} | less
fi

echo -n "Hint: the patch is right? wanna commit it? please enter 'yes/yes' or 'no/No': "
read git
if [[ $git == 'yes' || $git == 'Yes' ]];then
	cp -f $file $file_o
	rm -f $file
	git config --global push.default simple
	git add $file_o
	git commit -m "${results//:/ }"
	git push origin HEAD:master  -f
	if [[ $? != 0 ]];then
		echo "Error: git push failed! fix it! reset it! do it again!" 
		exit 2 
	fi
	echo "Hint: your yaml file is pushed to your fork hub"
	echo "do PR yourself, I'm tired of autoPR this script"
else 
	rm -f $file
	echo "Hint: you chose to exit :( "
	echo "Error: Sorry for bothering you, Please report mess to me"
	exit 2
fi
}

add_xml(){
#sync your xml  fork
if [[ ! -d $tmpx ]];then
        mkdir -p $tmpx
fi
# this is absolute path nono pushd
cd $tmpx
echo "Hint: xml hub lives $tmpx"
git clone git@gitee.com:${gitee_id}/manifest.git -b master
cd manifest
git remote add openeuler git@gitee.com:src-openeuler/manifest.git -f
git checkout -b oe openeuler/master  
git push origin oe:master -f
git checkout master 
git pull
echo "Hint: your xml fork is updated,we are now at `pwd`"

echo -n "Hint: add_xml for $@ , are you sure? please enter 'yes/yes' or 'no/No': "
read doo
if [[ "$doo" != 'yes' && "$doo" != 'Yes' ]];then
	echo "Hint: you chose to exit :( "
	exit 2
fi

file_o='default.xml'
file=${file_o}.cp
cp -f $file_o $file 
results='add'
# must use $@
for x in "$@";do
	a=$x
	cat $file | grep -w "${a}" &>/dev/null
	if [[ $? == 0 ]];then
		echo "$a has already existed,please check $file_o"
		continue
	fi
	
	#dude=`(cat $file && echo "- name: ${a}")|grep '^- name.*'|sort |grep -w $a -A1 |grep -v -w $a| cut -d' ' -f3`
	dude=`(cat default.xml|grep '^  <project.*' && echo "  <project path=\"openEuler/$a\" name=\"$a\"/>") | sort -t'"' -k4 |grep -w $a -A1 |grep -v -w $a| cut -d'"' -f4`
	targ=`cat -n $file |grep -w "$dude"| head -n1 |awk '{print $1}'`
	cat $file | sed "${targ}i\  <project path=\"openEuler/$a\" name=\"$a\"/>" >${file}.tmp 
	cp -f ${file}.tmp ${file}
	results=$results:$a
done
rm -f ${file}.tmp

echo -n "Hint: diff -Nur ${file_o} ${file}, please enter 'yes/Yes' or 'no/No': " 
read diff
if [[ $diff == 'yes' || $diff == 'Yes' ]];then
	diff -Nur ${file_o} ${file} | less
fi

echo -n "Hint: the patch is right? wanna commit it? please enter 'yes/yes' or 'no/No': "
read git
if [[ $git == 'yes' || $git == 'Yes' ]];then
	cp -f $file $file_o
	rm -f $file
	git config --global push.default simple
	git add $file_o
	git commit -m "${results//:/ }"
	git push origin HEAD:master  -f
	if [[ $? != 0 ]];then
		echo "Error: git push failed! fix it! reset it! do it again!" 
		exit 2 
	fi
	echo "Hint: your xml file is pushed to your fork hub"
	echo "do PR yourself, I'm tired of autoPR this script"
else 
	rm -f $file
	echo "Hint: you chose to exit :( "
	echo "Error: Sorry for bothering you, Please report mess change to me"
	exit 2
fi
}

main(){
if [[ $# == 0 ]];then
	echo "USAGE: `basename $0` GITEE_ID PKG ..."
	echo "EXAMP: `basename $0` sugarfillet pcp python-nasha"
	exit 2
fi

tmpy='/tmp/yaml'
tmpx='/tmp/xml'
gitee_id=$1
shift 1

add_yaml $@
sleep 3
add_xml $@

rm -rf $tmpy
rm -rf $tmpx

}

main $@
